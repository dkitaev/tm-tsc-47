package ru.tsc.kitaev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractCommand;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;

public class DataBase64LoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-load-base64";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load base64 data";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().dataBase64Load(session);
    }

}
