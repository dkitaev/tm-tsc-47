package ru.tsc.kitaev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractCommand;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;

public class DataJsonLoadJaxBCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-jaxb-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load json data from file";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().dataJsonLoadJaxB(session);
    }

}
