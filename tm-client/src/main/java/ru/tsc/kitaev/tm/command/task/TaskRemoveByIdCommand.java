package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id...";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter id");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("[REMOVE PROJECT BY ID]");
        serviceLocator.getTaskEndpoint().removeTaskById(session, id);
        System.out.println("[OK]");
    }

}
