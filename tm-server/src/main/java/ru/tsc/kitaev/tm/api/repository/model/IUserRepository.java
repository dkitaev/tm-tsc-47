package ru.tsc.kitaev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.User;

import java.util.List;

public interface IUserRepository {

    void add(@NotNull final User user);

    void update(@NotNull final User user);

    void clear();

    @NotNull
    List<User> findAll();

    @Nullable
    User findById(@NotNull final String id);

    @NotNull
    User findByIndex(@NotNull final Integer index);

    void removeById(@NotNull final String id);

    @NotNull
    User findByLogin(@NotNull final String login);

    @NotNull
    User findByEmail(@NotNull final String email);

    void removeByLogin(@NotNull final String login);

}
