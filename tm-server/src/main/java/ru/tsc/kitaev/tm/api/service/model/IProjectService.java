package ru.tsc.kitaev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void clear();

    void clear(@Nullable final String userId);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@Nullable final String userId);

    @NotNull
    List<Project> findAll(@Nullable final String userId, @Nullable final String sort);

    @NotNull
    Project findById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    Project findByIndex(@Nullable final String userId, @Nullable final Integer index);

    void removeById(@Nullable final String userId, @Nullable final String id);

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    boolean existsByIndex(@Nullable final String userId, final int index);

    @NotNull
    Integer getSize(@Nullable final String userId);

    void create(@Nullable final String userId, @Nullable final String name);

    @NotNull
    Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    );

    void addAll(@NotNull final List<Project> projects);

    @NotNull
    Project findByName(@Nullable final String userId, @Nullable final String name);

    void removeByName(@Nullable final String userId, @Nullable final String name);

    void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @NotNull final String description
    );

    void updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @NotNull String description
    );

    void startById(@Nullable final String userId, @Nullable final String id);

    void startByIndex(@Nullable final String userId, @Nullable final Integer index);

    void startByName(@Nullable final String userId, @Nullable final String name);

    void finishById(@Nullable final String userId, @Nullable final String id);

    void finishByIndex(@Nullable final String userId, @Nullable final Integer index);

    void finishByName(@Nullable final String userId, @Nullable final String name);

    void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    );

    void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    );

}
