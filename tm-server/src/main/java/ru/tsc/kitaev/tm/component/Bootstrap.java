package ru.tsc.kitaev.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.service.*;
import ru.tsc.kitaev.tm.api.service.dto.*;
import ru.tsc.kitaev.tm.api.service.model.*;
import ru.tsc.kitaev.tm.endpoint.*;
import ru.tsc.kitaev.tm.service.*;
import ru.tsc.kitaev.tm.service.dto.*;
import ru.tsc.kitaev.tm.service.model.*;
import ru.tsc.kitaev.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IProjectDTOService projectDTOService = new ProjectDTOService(connectionService, logService);

    @NotNull
    private final IProjectTaskDTOService projectTaskDTOService = new ProjectTaskDTOService(connectionService, logService);

    @NotNull
    private final ISessionDTOService sessionDTOService = new SessionDTOService(connectionService, logService, this);

    @NotNull
    private final ITaskDTOService taskDTOService = new TaskDTOService(connectionService, logService);

    @NotNull
    private final IUserDTOService userDTOService = new UserDTOService(connectionService, logService, propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService, logService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService, logService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, logService, this);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService, logService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, logService, propertyService);

    @NotNull
    private final Backup backup = new Backup(domainService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    public void start(@Nullable final String[] args) {
        try {
            initPID();
            initEndpoint();
            backup.init();
            } catch (@NotNull final Exception e) {
                logService.error(e);
                System.exit(1);
            }
    }

    private void initEndpoint() {
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(projectTaskEndpoint);
        registry(userEndpoint);
        registry(adminEndpoint);
        registry(adminUserEndpoint);
        registry(sessionEndpoint);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final Integer port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
