package ru.tsc.kitaev.tm.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractException extends RuntimeException {

    public AbstractException() {
        super();
    }

    public AbstractException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractException(@NotNull String s) {
        super(s);
    }

}
