package ru.tsc.kitaev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.model.ITaskRepository;
import ru.tsc.kitaev.tm.api.repository.model.IUserRepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.ILogService;
import ru.tsc.kitaev.tm.api.service.model.ITaskService;
import ru.tsc.kitaev.tm.enumerated.Sort;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.repository.model.TaskRepository;
import ru.tsc.kitaev.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskService implements ITaskService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    public TaskService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService
    ) {
        this.connectionService = connectionService;
        this.logService = logService;
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clearByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findAll();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findAllByUserId(userId);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable final String sort) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return Collections.emptyList();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<Task> comparator = sortType.getComparator();
            return taskRepository.findAllByUserId(userId).stream().sorted(comparator).collect(Collectors.toList());
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findById(userId, id);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @Nullable final Task task = taskRepository.findById(userId, id);
            return task != null;
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsByIndex(@Nullable final String userId, final int index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.findByIndex(userId, index);
            return true;
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Integer getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            return taskRepository.getSize(userId);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) throw new EntityNotFoundException();
            task.setUser(user);
            entityManager.getTransaction().begin();
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) throw new EntityNotFoundException();
            task.setUser(user);
            entityManager.getTransaction().begin();
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public void addAll(@NotNull final List<Task> tasks) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            for (Task task : tasks) {
                taskRepository.add(task);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findByName(userId, name);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @NotNull final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @Nullable final Task task = findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            task.setDescription(description);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @NotNull final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final Task task = findByIndex(userId, index);
            task.setName(name);
            task.setDescription(description);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @Nullable final Task task = findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final Task task = findByIndex(userId, index);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final Task task = findByName(userId, name);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @Nullable final Task task = findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setStatus(Status.COMPLETED);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }



    @Override
    public void finishByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final Task task = findByIndex(userId, index);
            task.setStatus(Status.COMPLETED);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final Task task = findByName(userId, name);
            task.setStatus(Status.COMPLETED);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @Nullable final Task task = findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setStatus(status);
            task.setStartDate(new Date());
            if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final Task task = findByIndex(userId, index);
            task.setStatus(status);
            task.setStartDate(new Date());
            if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final Task task = findByName(userId, name);
            task.setStatus(status);
            task.setStartDate(new Date());
            if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

}
