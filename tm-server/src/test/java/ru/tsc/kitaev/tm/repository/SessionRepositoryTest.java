package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.kitaev.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.service.ConnectionService;
import ru.tsc.kitaev.tm.service.PropertyService;
import ru.tsc.kitaev.tm.util.HashUtil;

import java.sql.SQLException;

public class SessionRepositoryTest {

    @NotNull
    private final SqlSession sqlSession;

    @NotNull
    private final ISessionDTORepository sessionRepository;

    @NotNull
    private final IUserDTORepository userRepository;

    @NotNull
    private final SessionDTO session;

    @NotNull
    private final String sessionId;

    @NotNull
    private final String userId;

    public SessionRepositoryTest() throws SQLException {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        sqlSession = connectionService.getSqlSession();
        userRepository = sqlSession.getMapper(IUserDTORepository.class);
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("test");
        userId = user.getId();
        @NotNull final String password = "test";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(secret, iteration, password));
        userRepository.add(user);
        sqlSession.commit();
        sessionRepository = sqlSession.getMapper(ISessionDTORepository.class);
        session = new SessionDTO();
        sessionId = session.getId();
        session.setUserId(userId);
        session.setTimestamp(System.currentTimeMillis());
    }

    @Before
    public void before() {
    }

    @Test
    public void openCloseTest() throws SQLException {
        sessionRepository.add(session);
        sqlSession.commit();
        @NotNull final SessionDTO tempSession = sessionRepository.findById(sessionId);
        Assert.assertEquals(session.getId(), tempSession.getId());
        Assert.assertEquals(session.getUserId(), tempSession.getUserId());
        Assert.assertEquals(session.getSignature(), tempSession.getSignature());
        sessionRepository.removeById(session.getId());
        sqlSession.commit();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @After
    public void after() throws SQLException {
        sessionRepository.clear();
        userRepository.removeById(session.getUserId());
        sqlSession.commit();
    }

}
