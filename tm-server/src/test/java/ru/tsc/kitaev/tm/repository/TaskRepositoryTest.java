package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kitaev.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.kitaev.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.TaskDTO;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.service.ConnectionService;
import ru.tsc.kitaev.tm.service.PropertyService;
import ru.tsc.kitaev.tm.util.HashUtil;

public class TaskRepositoryTest {

    @NotNull
    private final SqlSession sqlSession;

    @NotNull
    private final ITaskDTORepository taskRepository;

    @NotNull
    private final IProjectDTORepository projectRepository;

    @NotNull
    private final IUserDTORepository userRepository;

    @NotNull
    private final TaskDTO task;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private final String taskDescription = "testTaskDescription";

    @NotNull
    private final ProjectDTO project;

    @NotNull
    private final String projectId;

    @NotNull
    private final static String PROJECT_NAME = "testProject";

    @NotNull
    private final String userId;

    public TaskRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        sqlSession = connectionService.getSqlSession();
        taskRepository = sqlSession.getMapper(ITaskDTORepository.class);
        userRepository = sqlSession.getMapper(IUserDTORepository.class);
        projectRepository = sqlSession.getMapper(IProjectDTORepository.class);
        @NotNull final UserDTO user = new UserDTO();
        userId = user.getId();
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt("test", 5, "test"));
        userRepository.add(user);
        task = new TaskDTO();
        taskId = task.getId();
        task.setUserId(userId);
        task.setName(taskName);
        task.setDescription(taskDescription);
        project = new ProjectDTO();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(PROJECT_NAME);
        sqlSession.commit();
    }

    @Before
    public void before() {
        taskRepository.add(task);
        projectRepository.add(project);
        sqlSession.commit();
    }

    @Test
    public void findProjectTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskName);
        Assert.assertEquals(task, taskRepository.findById(userId, taskId));
        Assert.assertEquals(task, taskRepository.findByIndex(userId, 0));
        Assert.assertEquals(task, taskRepository.findByName(userId, taskName));
    }

    @Test
    public void removeTaskByIdTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskRepository.removeById(userId, taskId);
        sqlSession.commit();
        Assert.assertTrue(taskRepository.findAllByUserId(userId).isEmpty());
    }

    @Test
    public void removeTaskByIndexTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        taskRepository.removeByIndex(userId, 0);
        sqlSession.commit();
        Assert.assertTrue(taskRepository.findAllByUserId(userId).isEmpty());
    }

    @Test
    public void removeTaskByNameTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskRepository.removeByName(userId, taskName);
        sqlSession.commit();
        Assert.assertTrue(taskRepository.findAllByUserId(userId).isEmpty());
    }

    @Test
    public void updateByIdTest() {
        @NotNull final String newName = "newTestTask";
        @NotNull final String newDescription = "newTestTaskDescription";
        taskRepository.updateById(userId, taskId, newName, newDescription);
        sqlSession.commit();
        Assert.assertEquals(newName, taskRepository.findById(userId, projectId).getName());
        Assert.assertEquals(newDescription, taskRepository.findById(userId, projectId).getDescription());
        Assert.assertNotEquals(taskName, taskRepository.findById(userId, projectId).getName());
        Assert.assertNotEquals(taskDescription, taskRepository.findById(userId, projectId).getDescription());
    }

    @Test
    public void updateByIndexTest() {
        @NotNull final String newName = "newTestTask";
        @NotNull final String newDescription = "newTestTaskDescription";
        taskRepository.updateByIndex(userId, 0, newName, newDescription);
        sqlSession.commit();
        Assert.assertEquals(newName, taskRepository.findById(userId, projectId).getName());
        Assert.assertEquals(newDescription, taskRepository.findById(userId, projectId).getDescription());
        Assert.assertNotEquals(taskName, taskRepository.findById(userId, projectId).getName());
        Assert.assertNotEquals(taskDescription, taskRepository.findById(userId, projectId).getDescription());
    }

    @Test
    public void startByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskRepository.startById(userId, taskId, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    public void startByIndexTest() {
        Assert.assertNotNull(userId);
        taskRepository.startByIndex(userId, 0, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void startByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskRepository.startByName(userId, taskName, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    public void finishByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskRepository.finishById(userId, taskId, Status.COMPLETED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    public void finishByIndexTest() {
        Assert.assertNotNull(userId);
        taskRepository.finishByIndex(userId, 0, Status.COMPLETED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void finishByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskRepository.finishByName(userId, taskName, Status.COMPLETED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    public void changeStatusByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskRepository.changeStatusById(userId, taskId, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findById(userId, taskId).getStatus());
        taskRepository.changeStatusById(userId, taskId, Status.COMPLETED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findById(userId, taskId).getStatus());
        taskRepository.changeStatusById(userId, taskId, Status.NOT_STARTED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    public void changeStatusByIndexTest() {
        Assert.assertNotNull(userId);
        taskRepository.changeStatusByIndex(userId, 0, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByIndex(userId, 0).getStatus());
        taskRepository.changeStatusByIndex(userId, 0, Status.COMPLETED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByIndex(userId, 0).getStatus());
        taskRepository.changeStatusByIndex(userId, 0, Status.NOT_STARTED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void changeStatusByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskRepository.changeStatusByName(userId, taskName, Status.IN_PROGRESS.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByName(userId, taskName).getStatus());
        taskRepository.changeStatusByName(userId, taskName, Status.COMPLETED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByName(userId, taskName).getStatus());
        taskRepository.changeStatusByName(userId, taskName, Status.NOT_STARTED.toString());
        sqlSession.commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    public void findAllTaskByProjectIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        sqlSession.commit();
        Assert.assertEquals(task, taskRepository.findAllTaskByProjectId(userId, projectId).get(0));
    }

    @Test
    public void bindTaskToProjectByIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        sqlSession.commit();
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void unbindTaskByIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        sqlSession.commit();
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
        taskRepository.unbindTaskById(userId, projectId, taskId);
        sqlSession.commit();
        Assert.assertNull(taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void removeAllTaskByProjectIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        @NotNull final TaskDTO task1 = new TaskDTO();
        task1.setName("testTask1");
        task1.setUserId(userId);
        taskRepository.add(task1);
        taskRepository.bindTaskToProjectById(userId, projectId, task1.getId());
        sqlSession.commit();
        Assert.assertEquals(2, taskRepository.findAllTaskByProjectId(userId, projectId).size());
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        sqlSession.commit();
        Assert.assertEquals(0, taskRepository.findAllTaskByProjectId(userId, projectId).size());
    }

    @After
    public void after() {
        taskRepository.clearByUserId(userId);
        projectRepository.clearByUserId(userId);
        userRepository.removeById(userId);
        sqlSession.commit();
        sqlSession.close();
    }

}
