package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.service.ConnectionService;
import ru.tsc.kitaev.tm.service.PropertyService;
import ru.tsc.kitaev.tm.util.HashUtil;

public class UserRepositoryTest {

    @NotNull
    private final SqlSession sqlSession;

    @NotNull
    private final IUserDTORepository userRepository;

    @NotNull
    private final UserDTO user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final String userEmail = "userTest@mail.com";

    public UserRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        sqlSession = connectionService.getSqlSession();
        userRepository = sqlSession.getMapper(IUserDTORepository.class);
        user = new UserDTO();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        @NotNull final String password = "userTest";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(secret, iteration, password));
    }

    @Before
    public void before() {
        userRepository.add(user);
        sqlSession.commit();
    }

    @Test
    public void findByUserTest() {
        Assert.assertEquals(user, userRepository.findById(userId));
        Assert.assertEquals(user, userRepository.findByIndex(0));
        Assert.assertEquals(user, userRepository.findByLogin(userLogin));
        Assert.assertEquals(user, userRepository.findByEmail(userEmail));
    }

    @Test
    public void removeUserTest() {
        userRepository.removeById(user.getId());
        sqlSession.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByLoginTest() {
        userRepository.removeByLogin(userLogin);
        sqlSession.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @After
    public void after() {
        userRepository.removeById(userId);
        sqlSession.commit();
    }

}
