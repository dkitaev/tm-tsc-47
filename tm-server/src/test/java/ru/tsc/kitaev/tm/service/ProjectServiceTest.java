package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kitaev.tm.api.service.dto.IUserDTOService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.service.dto.ProjectDTOService;
import ru.tsc.kitaev.tm.service.dto.UserDTOService;

public class ProjectServiceTest {

    @NotNull
    private final IProjectDTOService projectService;

    @NotNull
    private final IUserDTOService userService;

    @NotNull
    private final ProjectDTO project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String projectDescription = "testProjectDescription";

    @NotNull
    private final String userId;

    public ProjectServiceTest() {
        projectService = new ProjectDTOService(new ConnectionService(new PropertyService()), new LogService());
        userService = new UserDTOService((new ConnectionService(new PropertyService())), new LogService(), new PropertyService());
        userId = userService.create("test", "test").getId();
        project = projectService.create(userId, projectName, projectDescription);
        projectId = project.getId();
    }

    @Test
    public void createTest() {
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        @NotNull final ProjectDTO newProject = projectService.create(userId, newProjectName, newProjectDescription);
        Assert.assertEquals(2, projectService.findAll(userId).size());
        Assert.assertEquals(newProjectName, newProject.getName());
        Assert.assertEquals(newProjectDescription, newProject.getDescription());
    }

    @Test
    public void findByProjectTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertEquals(project, projectService.findById(userId, projectId));
        Assert.assertEquals(project, projectService.findByIndex(userId, 0));
        Assert.assertEquals(project, projectService.findByName(userId, projectName));
    }

    @Test
    public void updateByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectDescription);
        Assert.assertEquals(project, projectService.findByName(userId, projectName));
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        projectService.updateById(userId, projectId, newProjectName, newProjectDescription);
        Assert.assertEquals(project, projectService.findByName(userId, newProjectName));
        @NotNull final ProjectDTO newProject = projectService.findByName(userId, newProjectName);
        Assert.assertEquals(userId, newProject.getUserId());
        Assert.assertEquals(projectId, newProject.getId());
        Assert.assertEquals(newProjectName, newProject.getName());
        Assert.assertEquals(newProjectDescription, newProject.getDescription());
    }

    @Test
    public void updateByIndexTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectDescription);
        Assert.assertEquals(project, projectService.findById(userId, projectId));
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        projectService.updateByIndex(userId, 0,newProjectName, newProjectDescription);
        Assert.assertEquals(project, projectService.findByIndex(userId, 0));
        @NotNull final ProjectDTO newProject = projectService.findByIndex(userId, 0);
        Assert.assertEquals(userId, newProject.getUserId());
        Assert.assertEquals(projectId, newProject.getId());
        Assert.assertEquals(newProjectName, newProject.getName());
        Assert.assertEquals(newProjectDescription, newProject.getDescription());
    }

    @Test
    public void removeProjectByIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.removeById(userId, projectId);
        Assert.assertTrue(projectService.findAll(userId).isEmpty());
    }

    @Test
    public void removeProjectByIndexTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        projectService.removeByIndex(userId, 0);
        Assert.assertTrue(projectService.findAll(userId).isEmpty());
    }

    @Test
    public void removeProjectByNameTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.removeByName(userId, projectName);
        Assert.assertTrue(projectService.findAll(userId).isEmpty());
    }

    @Test
    public void startByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.startById(userId, projectId);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    public void startByIndexTest() {
        Assert.assertNotNull(userId);
        projectService.startByIndex(userId, 0);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void startByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.startByName(userId, projectName);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByName(userId, projectName).getStatus());
    }

    @Test
    public void finishByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.finishById(userId, projectId);
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    public void finishByIndexTest() {
        Assert.assertNotNull(userId);
        projectService.finishByIndex(userId, 0);
        Assert.assertEquals(Status.COMPLETED, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void finishByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.finishByName(userId, projectName);
        Assert.assertEquals(Status.COMPLETED, projectService.findByName(userId, projectName).getStatus());
    }

    @Test
    public void changeStatusByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.changeStatusById(userId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(userId, projectId).getStatus());
        projectService.changeStatusById(userId, projectId, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectId).getStatus());
        projectService.changeStatusById(userId, projectId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    public void changeStatusByIndexTest() {
        Assert.assertNotNull(userId);
        projectService.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByIndex(userId, 0).getStatus());
        projectService.changeStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findByIndex(userId, 0).getStatus());
        projectService.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void changeStatusByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.changeStatusByName(userId, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByName(userId, projectName).getStatus());
        projectService.changeStatusByName(userId, projectName, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findByName(userId, projectName).getStatus());
        projectService.changeStatusByName(userId, projectName, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findByName(userId, projectName).getStatus());
    }

    @After
    public void after() {
        projectService.removeById(userId, projectId);
        userService.removeById(userId);
    }

}
