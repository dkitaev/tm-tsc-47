package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.dto.IUserDTOService;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.service.dto.UserDTOService;
import ru.tsc.kitaev.tm.util.HashUtil;

public class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserDTOService userService;

    @NotNull
    private final UserDTO user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final String userEmail = "test@mail.com";

    public UserServiceTest() {
        propertyService = new PropertyService();
        userService = new UserDTOService(new ConnectionService(new PropertyService()), new LogService(), new PropertyService());
        user = userService.create(userLogin, "userTest", userEmail);
        userId = user.getId();
    }

    @Test
    public void findByUserTest() {
        Assert.assertEquals(user, userService.findById(userId));
        Assert.assertEquals(user, userService.findByIndex(0));
        Assert.assertEquals(user, userService.findByLogin(userLogin));
        Assert.assertEquals(user, userService.findByEmail(userEmail));
    }

    @Test
    public void removeByLoginTest() {
        userService.removeByLogin(userLogin);
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void createTest() {
        Assert.assertEquals(1, userService.findAll().size());
        @NotNull final String newUserLogin = "newUserTest";
        @NotNull final String newUserEmail = "newTest@mail.com";
        @NotNull final String newUserPassword = "newPassword";
        @NotNull final UserDTO newUser = userService.create(newUserLogin, newUserPassword, newUserEmail);
        Assert.assertEquals(newUserLogin, newUser.getLogin());
        Assert.assertEquals(newUserEmail, newUser.getEmail());
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        Assert.assertEquals(HashUtil.salt(secret, iteration, newUserPassword), newUser.getPasswordHash());
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    public void setPasswordTest() {
        Assert.assertEquals(user.getPasswordHash(), HashUtil.salt("test", 5, "test"));
        @NotNull final String newPassword = "newPassword";
        userService.setPassword(userId, newPassword);
        Assert.assertNotEquals(HashUtil.salt("test", 5, "test"), user.getPasswordHash());
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        Assert.assertEquals(HashUtil.salt(secret, iteration, newPassword), user.getPasswordHash());
    }

    @Test
    public void updateUserTest() {
        @NotNull final String newFirstName = "newFirstNameTest";
        @NotNull final String newLastName = "newLastNameTest";
        @NotNull final String newMiddleName = "newMiddleNameTest";
        Assert.assertNull(user.getLastName());
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getMiddleName());
        userService.updateUser(userId, newFirstName, newLastName, newMiddleName);
        Assert.assertEquals(newFirstName, user.getFirstName());
        Assert.assertEquals(newLastName, user.getLastName());
        Assert.assertEquals(newMiddleName, user.getMiddleName());
    }

    @Test
    public void lockUnlockByLoginTest() {
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(userLogin);
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin(userLogin);
        Assert.assertFalse(user.getLocked());
    }

    @After
    public void after() {
        userService.removeById(userId);
    }

}
